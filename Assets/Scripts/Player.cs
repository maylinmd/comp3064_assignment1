﻿/*********************************************************
 * File name: Player 
 * Author: Maylin Morales Diaz
 * Student No: 100915710
 * Last Modified by: Maylin Morales Diaz
 * Date last Modified: 2016/10/29
 * Program description: This script creates a player object
 * *****************************************************/ 
using UnityEngine;
using System.Collections;

public class Player {

	private const string key = "MAX_SCORE";

	//Creating singleton class
	private Player(){
		//get highScore from the PlayerPrefs if exists
		if (PlayerPrefs.HasKey (key)) {
			_highScore = PlayerPrefs.GetInt (key);
		}

	}

	private static Player _myPlayer = null;

	//Creating singleton class
	public static Player MyPlayer{

		get{ 
			if (_myPlayer == null) {
				_myPlayer = new Player ();
			}

			return _myPlayer;
		}
	}

	//Instance of HUDController
	public HUDController hud = null;

	//Points collected by the player
	private int _playerPoints = 0;
	public int PlayerPoints{
		get{ 
			return _playerPoints;
		}

		set{ 
			_playerPoints = value;
			HighScore = _playerPoints;
			//Updates the points collected by player
			hud.UpdatePoints ();
		}
	}

	private int _highScore = 0;

	public int HighScore{
		get{ 
			return _highScore;
		}

		set{ 
			if (value > _highScore) {
				_highScore = value;
				PlayerPrefs.SetInt (key, _highScore);
			}
		}
	}

	//Maximun player lives set to 5
	private int _playerLives = 5;
	public int PlayerLives{
		get{ 
			return _playerLives;
		}

		set{ 
			_playerLives = value;

			//Updates the number of lives of the player
			hud.UpdateLives ();
			if (_playerLives <= 0) {
				hud.GameOver ();
			}
		}
	}

}
