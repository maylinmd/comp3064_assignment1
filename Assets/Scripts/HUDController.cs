﻿/*********************************************************
 * File name: HUDController
 * Author: Maylin Morales Diaz
 * Student No: 100915710
 * Last Modified by: Maylin Morales Diaz
 * Date last Modified: 2016/10/29
 * Program description: This script handles game over
 * 						and restart game
 * *****************************************************/ 
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDController : MonoBehaviour {

	[SerializeField]
	Text pointsLabel = null;

	[SerializeField]
	Text livesLabel = null;

	[SerializeField]
	Button playAgainBtn = null;

	[SerializeField]
	Text gameOverLabel = null;

	[SerializeField]
	GameObject player = null;

	[SerializeField]
	Text maxPointslabel = null;

	// Initialization 
	void Start () {
		pointsLabel.text = "Points: 0";
		livesLabel.text = "Lives: 5";

		Player.MyPlayer.hud = this;

		RestartGame ();
	}

	//Update the points label
	public void UpdatePoints(){

		pointsLabel.text = "Points: " + Player.MyPlayer.PlayerPoints;
	}

	//Update the lives label
	public void UpdateLives(){

		livesLabel.text = "Lives: " + Player.MyPlayer.PlayerLives;
	}

	//Finish the game 
	public void GameOver(){

		//Hide the labels for lives and points
		pointsLabel.gameObject.SetActive(false);
		livesLabel.gameObject.SetActive (false);

		//Deactivate player
		player.SetActive(false);

		//Display Game Over Label
		gameOverLabel.gameObject.SetActive(true);

		//Display total points collected label
		maxPointslabel.gameObject.SetActive (true);
		maxPointslabel.text = "Highest Score: " + Player.MyPlayer.HighScore;

		//Display Play Again button
		playAgainBtn.gameObject.SetActive(true);

	}

	//Restart the game
	public void RestartGame(){

		//Display the labels for lives and points
		pointsLabel.gameObject.SetActive(true);
		livesLabel.gameObject.SetActive (true);

		//Reset lives and points to initial values
		Player.MyPlayer.PlayerPoints = 0;
		Player.MyPlayer.PlayerLives = 5;

		//Hides the total points collected label
		maxPointslabel.gameObject.SetActive (false);

		//Activate player
		player.SetActive(true);

		//Hide Game Over Label
		gameOverLabel.gameObject.SetActive(false);

		//Hide Play button
		playAgainBtn.gameObject.SetActive(false);
	}


}
