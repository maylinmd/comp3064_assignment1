﻿/*********************************************************
 * File name: Shark Collider
 * Author: Maylin Morales Diaz
 * Student No: 100915710
 * Last Modified by: Maylin Morales Diaz
 * Date last Modified: 2016/10/29
 * Program description: This script handles collision
 *                     of avatar with enemies and price
 * *****************************************************/ 
using UnityEngine;
using System.Collections;

public class SharkCollider : MonoBehaviour {

	[SerializeField]
	GameObject bloodSplash = null;

	AudioSource eatAudio;
	AudioSource splashAudio;

	void Start(){
		//Array that contains two audio sources for shark game object
		AudioSource[] audios = GetComponents<AudioSource>();

		//Assign the first audio source to variable
		splashAudio = audios [0];

		//Assign the second audio source to variable
		eatAudio = audios [1];

	}
	//On trigger with other game objects
	void OnTriggerEnter2D(Collider2D other){

		//If shark collides with price(fish)
		if (other.gameObject.tag == "fish") {

			Debug.Log ("Collision with " + other.gameObject.tag);
			
			//Increase player points on collision with fish
			Player.MyPlayer.PlayerPoints += 5;

			FishController fish = other.gameObject.GetComponent<FishController> ();

			if (fish != null) {
				//Reset fish
				fish.Reset ();
			}

			AudioSource audioSource = gameObject.GetComponent<AudioSource> ();
			if (audioSource != null) {
				//Play audio source
				eatAudio.Play ();
			}

		} 

		//If shark collides with enemy(can)
		else if (other.gameObject.tag == "can") {
			//Debuging Collision
			Debug.Log ("Collision with " +
				other.gameObject.tag);

			//Decrease player lives by 1 on collision with can
			Player.MyPlayer.PlayerLives -= 1;

			CanController can = other.gameObject.GetComponent<CanController> ();

			if (can != null) {

				//show blood splash animation
				GameObject splash = 
					Instantiate (bloodSplash);
				splash.transform.position = can.transform.position;

				//Reset can
				can.Reset ();
			}

			AudioSource audioSource = gameObject.GetComponent<AudioSource> ();

			if (audioSource != null) {
				//Play audio source
				splashAudio.Play ();
			}

		}

	}
}

