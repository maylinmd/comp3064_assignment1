﻿/*********************************************************
 * File name: Blood Splash Controller
 * Author: Maylin Morales Diaz
 * Student No: 100915710
 * Last Modified by: Maylin Morales Diaz
 * Date last Modified: 2016/10/29
 * Program description: Destroy the blood splash animation
 * *****************************************************/ 
using UnityEngine;
using System.Collections;

public class BloodSplashController : MonoBehaviour {
	//Destroy bubbles animation
	public void End(){
		//Destroy the game object
		Destroy (gameObject);

	}
}
