﻿/*********************************************************
 * File name: Shark Controller
 * Author: Maylin Morales Diaz
 * Student No: 100915710
 * Last Modified by: Maylin Morales Diaz
 * Date last Modified: 2016/10/29
 * Program description: This script controls the avatar
 * *****************************************************/ 
using UnityEngine;
using System.Collections;

public class SharkController : MonoBehaviour {

	[SerializeField]
	private float speed;

	private Transform _transform;
	private Vector2 _currentPosition;
	private float _playerControl;

	// On initialization 
	void Start () {

		_transform = gameObject.GetComponent<Transform>();
		_currentPosition = _transform.position;
	}

	// Update is called once per frame
	void FixedUpdate () {

		_playerControl = Input.GetAxis ("Vertical");
		_currentPosition = _transform.position;
		//move up
		if (_playerControl > 0) {
			_currentPosition += new Vector2 (0, speed);
		}
		//move down
		if (_playerControl < 0) {
			_currentPosition -= new Vector2 (0, speed);
		}
		//fix bounds
		fixBounds ();
		_transform.position = _currentPosition;
	}

	//Fix the bounds of the shark
	private void fixBounds(){

		if (_currentPosition.y < -4f) {
			_currentPosition.y = -4f;
		}
		if (_currentPosition.y > 4f) {
			_currentPosition.y = 4f;
		}
	}
}
