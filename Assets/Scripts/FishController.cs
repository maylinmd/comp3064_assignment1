﻿/************************************************************
 * File name: Fish Controller
 * Author: Maylin Morales Diaz
 * Student No: 100915710
 * Last Modified by: Maylin Morales Diaz
 * Date last Modified: 2016/10/29
 * Program description: This script controls the fish actions
 * ***********************************************************/ 
using UnityEngine;
using System.Collections;

public class FishController : MonoBehaviour {

	[SerializeField]
	private float speed;

	private Transform _transform;
	private Vector2 _currentPosition;

	private float miny = -3f;
	private float maxy = 3f;

	// Use this for initialization
	void Start () {

		_transform = gameObject.GetComponent<Transform>();
		_currentPosition = _transform.position;
		Reset ();
	}

	// Update is called once per frame
	void FixedUpdate () {
		_currentPosition = _transform.position;

		_currentPosition -= new Vector2 (speed, 0);
		_transform.position = _currentPosition;

		if (_currentPosition.x <= -5.3) {
			Reset ();
		}
	}

	//Reset fish position
	public void Reset(){
		float xPosition = Random.Range (5.3f, 20f);
		float yPosition = Random.Range (miny, maxy);
		_currentPosition = new Vector2 (xPosition, yPosition);
		_transform.position = _currentPosition;
	}


}
